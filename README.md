# Simple List with CRUD Application

[API src](https://reqres.in/)

## Deliverables:

- Use ReactJS and Redux for this
- Implement a List view with basic CRUD capabilities
- You can use any 3rd party libraries you want
- Create a your own branch from dev and make a PR to master

## Bonus points:

- A plus if written with Context API w/ hooks
- Host it on heroku
- Basic unit testing
